//
//  config.h
//  golden_rule
//
//  Created by Khaled Qudwa on 8/14/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#ifndef golden_rule_config_h
#define golden_rule_config_h
#define LWAppMainColor [UIColor colorWithRed:0.211765 green:0.639216 blue:0.921569 alpha:1.0]
#define LWAppFontName @"TheSans"
#define MainScreenWidth [UIScreen mainScreen].bounds.size.width
#endif
