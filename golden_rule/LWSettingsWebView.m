//
//  LWSettingsWebView.m
//  golden_rule
//
//  Created by Khaled Qudwa on 8/10/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import "LWSettingsWebView.h"
#import "config.h"
@interface LWSettingsWebView ()

@end

@implementation LWSettingsWebView
{
    UIWebView *contentView;
}
@synthesize naviTitle, fileName;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.naviTitle;
    self.view.backgroundColor = [UIColor whiteColor];
    contentView = [[UIWebView alloc] initWithFrame:CGRectMake(10.0, 75.0,
                                                              self.view.frame.size.width-20.0,
                                                              self.view.frame.size.height-90.0)];
    contentView.layer.borderColor = LWAppMainColor.CGColor;
    contentView.layer.borderWidth = 1.0;
    contentView.layer.cornerRadius = 3.5;
    contentView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
    UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin |
    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight |
    UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:contentView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.toolbarHidden = YES;
    self.navigationController.navigationBarHidden = NO;
    NSString *bodyHTML = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:self.fileName ofType:@"html"] encoding:NSUTF8StringEncoding error:nil];
    
    NSLog(@"%@",[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"guidelines" ofType:@"html"]]);
    [contentView loadHTMLString:bodyHTML baseURL:nil];
    
}

@end