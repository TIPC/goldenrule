//
//  LWDetailsCVC.m
//  golden_rule
//
//  Created by Khaled Qudwa on 8/10/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//


#import "LWDetailsCVC.h"
#import "LWDetailsSectionHeader.h"
#import "LWDetailsCell.h"
#import "UIImageView+WebCache.h"
#import "config.h"
#import "LWSettingsWebView.h"
@import AVFoundation;

@interface LWDetailsCVC ()<UIAlertViewDelegate, UIActionSheetDelegate>

@end

@implementation LWDetailsCVC
{
    NSArray *pageSections;
    AVAudioPlayer *soundPlayer;
    
    UIBarButtonItem *nextBtn;
    UIBarButtonItem *previousBtn;
    UIBarButtonItem *pageBarLbl;
    
    NSMutableArray *doubleSizeItems;
}
@synthesize page, db;

-(UICollectionViewFlowLayout *)layout
{
    UICollectionViewFlowLayout *flowLayout;
    flowLayout = UICollectionViewFlowLayout.new;
    flowLayout.minimumLineSpacing = 5;
    flowLayout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.headerReferenceSize = CGSizeMake(MainScreenWidth, 50);
    return flowLayout;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout  *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([doubleSizeItems indexOfObject:indexPath] != NSNotFound)
        return CGSizeMake(140.0, 70);
    else
        return CGSizeMake(70, 70);
}

-(instancetype)init
{
    self = [super initWithCollectionViewLayout:self.layout];
    
    UINib *headerNib = [UINib nibWithNibName:@"LWDetailsSectionHeader" bundle:nil];
    [self.collectionView registerNib:headerNib
          forSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                 withReuseIdentifier:@"sectionHeader"];
    
    UINib *cellNib = [UINib nibWithNibName:@"LWDetailsCell" bundle:nil];
    [self.collectionView registerNib:cellNib
          forCellWithReuseIdentifier:@"reusableCell"];

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];

    self.collectionView.transform = CGAffineTransformMakeScale(-1, 1);
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.collectionView.backgroundColor = [UIColor clearColor];
    UIImageView *backgroundImg = UIImageView.new;
    backgroundImg.image = [UIImage imageNamed:@"GR_main_bg"];
    backgroundImg.transform = self.collectionView.transform;
    self.collectionView.backgroundView = backgroundImg;

    nextBtn = [[UIBarButtonItem alloc] initWithTitle:@"التالي" style:UIBarButtonItemStylePlain target:self action:@selector(nextPage:)];
    previousBtn = [[UIBarButtonItem alloc] initWithTitle:@"السابق" style:UIBarButtonItemStylePlain target:self action:@selector(previousPage:)];
    pageBarLbl = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:@selector(selectPage:)];
    nextBtn.tintColor = previousBtn.tintColor = pageBarLbl.tintColor = LWAppMainColor;
    self.toolbarItems = @[
                          nextBtn,
                          [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                          pageBarLbl,
                          [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                          previousBtn];
    
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"shortcut"] style:UIBarButtonItemStyleDone target:self action:@selector(settings:)];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.toolbarHidden = NO;
    self.navigationController.navigationBarHidden = NO;
    
    [self refereshPageData];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                atScrollPosition:UICollectionViewScrollPositionTop
                                        animated:YES];

}
-(IBAction)settings:(id)sender
{
    [[[UIActionSheet alloc] initWithTitle:@""
                                 delegate:self
                        cancelButtonTitle:@"إلغاء"
                   destructiveButtonTitle:nil
                        otherButtonTitles:@"حول القاعدة الذهبية",
      @"دليل معلم القاعدة الذهبية",
      @"إرشادات",nil] showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex < 3)
    {
        LWSettingsWebView *settingView = LWSettingsWebView.new;
        settingView.naviTitle = @[@"حول القاعدة الذهبية",
                                  @"دليل معلم القاعدة الذهبية",
                                  @"إرشادات"][buttonIndex];
        settingView.fileName = @[@"DSA",@"tech_guide", @"guidelines"][buttonIndex];
        [self.navigationController pushViewController:settingView animated:YES];
    }
    
}
-(NSArray *)pageList
{
    return self.db[@"pages"];
}
-(void)refereshPageData
{
    doubleSizeItems = NSMutableArray.new;
    NSArray *pageList = self.pageList;
    NSArray *currentPageSections = pageList[self.page][@"sections"];
    NSMutableArray *tempAry = NSMutableArray.new;
    for (NSDictionary *tempDic in currentPageSections)
    {
        NSString *sectionTitle = @"DSA";
        NSString *lessonIndex = tempDic[@"lesson_index"];
        NSString *sectionIndex = tempDic[@"section_index"];
        if (sectionIndex.length == 0)
            sectionTitle = self.db[@"lessons"][lessonIndex.integerValue][@"name"];
        else
            sectionTitle = self.db[@"lessons"][lessonIndex.integerValue][@"sections"][sectionIndex.integerValue];

        [tempAry addObject:@{
                             @"title" : sectionTitle,
                             @"from" : tempDic[@"from"], @"to" : tempDic[@"to"] }];
    }
    pageSections = tempAry;

    self.navigationItem.title = self.db[@"lessons"][[currentPageSections.firstObject[@"lesson_index"] integerValue]][@"subtitle"];
    previousBtn.enabled = (self.page > 0);
    nextBtn.enabled = (self.pageNO < pageList.count);
    pageBarLbl.title = [NSString stringWithFormat:@"%@/%@", @(pageList.count),@(self.pageNO)];
    [self.collectionView reloadData];
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                atScrollPosition:UICollectionViewScrollPositionTop
                                        animated:YES];

}
-(IBAction)selectPage:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"أدخل رقم الصفحة"
                                                       delegate:self
                                              cancelButtonTitle:@"إلغاء"
                                              otherButtonTitles:@"إذهب", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeNumberPad];
    [alertView show];

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSInteger nextPage = [[alertView textFieldAtIndex:0].text integerValue]-1;
    
    if (buttonIndex == 1 && nextPage >= 0 && nextPage < self.pageList.count)
    {
        self.page = nextPage;
        [self refereshPageData];
    }
}
-(IBAction)nextPage:(id)sender
{
    self.page++;
    [self refereshPageData];
}

-(IBAction)previousPage:(id)sender
{
    self.page--;
    [self refereshPageData];
}

-(NSInteger)pageNO
{
    return self.page+1;
}
-(NSString *)imageURLAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger sectionStart = [pageSections[indexPath.section][@"from"] integerValue];
    NSInteger imgNO = sectionStart+indexPath.row;
    NSString *imageURL = [NSString stringWithFormat:@"http://www.almajdit.com/gr-1.0/images/%@/%@.png",@(self.pageNO), @(imgNO)];
    return imageURL;

}
-(NSString *)soundFileNameAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger sectionStart = [pageSections[indexPath.section][@"from"] integerValue];
    NSInteger itemNO = sectionStart+indexPath.row;
    NSString *soundFileName = [NSString stringWithFormat:@"%@-%@.wav",@(self.page),@(itemNO)];
    return soundFileName;
}
-(NSString *)soundURLAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger sectionStart = [pageSections[indexPath.section][@"from"] integerValue];
    NSInteger itemNO = sectionStart+indexPath.row;
    NSString *soundURL = [NSString stringWithFormat:@"http://almajdit.com/gr-1.0/sounds/%@/%@/%@.wav",@(1),@(self.pageNO),@(itemNO)];

    return soundURL;
}

-(NSString *)documentPath
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return pageSections.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    NSDictionary *sectionInfo = pageSections[section];
    return [sectionInfo[@"to"] integerValue] - [sectionInfo[@"from"] integerValue]+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    LWDetailsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"reusableCell" forIndexPath:indexPath];
    cell.transform = self.collectionView.transform;

    NSURL *imgURL = [NSURL URLWithString:[self imageURLAtIndexPath:indexPath]];
    [cell.cellImg sd_setImageWithURL:imgURL
                           completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                               CGFloat sizeFactor = image.size.width/image.size.height;
                               if (sizeFactor > 1.0 &&
                                   [doubleSizeItems indexOfObject:indexPath] == NSNotFound)
                               {
                                   [doubleSizeItems addObject:indexPath];
                                   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                       [self.collectionView reloadData];
                                   });
                                   
                               }
                           }];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
          viewForSupplementaryElementOfKind:(NSString *)kind
                                atIndexPath:(NSIndexPath *)indexPath
{
    LWDetailsSectionHeader *resuableView = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        resuableView = [collectionView
                        dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                        withReuseIdentifier:@"sectionHeader"
                        forIndexPath:indexPath];
        resuableView.sectionTitle.text = pageSections[indexPath.section][@"title"];
        resuableView.transform = self.collectionView.transform;
    }
    return resuableView;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *soundFileName = [self soundFileNameAtIndexPath:indexPath];
    NSString *filePath = [self.documentPath stringByAppendingPathComponent:soundFileName];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        
        NSData *musicData = [NSData dataWithContentsOfFile:filePath];
        NSError *error = nil;
        soundPlayer = [[AVAudioPlayer alloc] initWithData:musicData
                                                    error:&error];
        
        [soundPlayer play];
        NSLog(@"Offline %@",filePath);
        if (error != nil)
        {
            NSLog(@"Error Play");
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
            [self collectionView:collectionView didSelectItemAtIndexPath:indexPath];
        }
    }
    else
    {
        NSURL *soundURL = [NSURL URLWithString:[self soundURLAtIndexPath:indexPath]];
        NSLog(@"Online %@",soundURL);
        [NSURLConnection sendAsynchronousRequest:[NSURLRequest requestWithURL:soundURL]
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
        {
            NSLog(@"Donwload With Error %@",connectionError);
            if (data != nil)
            {
                [data writeToFile:filePath atomically:YES];
                soundPlayer = [[AVAudioPlayer alloc] initWithData:data
                                                            error:nil];
                [soundPlayer play];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"خطأ"
                                            message:@"حدث خطأ أثناء تحميل ملف الصوت من الإنترنت"
                                           delegate:nil
                                  cancelButtonTitle:@"موافق"
                                  otherButtonTitles:nil] show];
            }
        }];
    }
        
    
}




@end
