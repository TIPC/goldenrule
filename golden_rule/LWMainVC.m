//
//  LWMainVC.m
//  golden_rule
//
//  Created by Khaled Qudwa on 8/10/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import "LWMainVC.h"
#import "LWDetailsCVC.h"
#import "config.h"
@interface LWMainVC ()<UITableViewDataSource, UITableViewDelegate>

@end

@implementation LWMainVC
{
    NSArray *lessonsList;
    NSDictionary *database;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIView *upperView = [UIView new];
    upperView.frame = CGRectMake(0, 0, MainScreenWidth, 20.0);
    upperView.backgroundColor = LWAppMainColor;
    [self.view addSubview:upperView];
    
    self.navigationItem.backBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    mainList.backgroundColor =
    mainList.separatorColor = [UIColor clearColor];
    mainList.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIEdgeInsets tableEdge = mainList.contentInset;
    tableEdge.bottom = 145.0;
    mainList.contentInset = tableEdge;

    
    NSData *dbData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"db" ofType:@"json"]];
    database = [NSJSONSerialization JSONObjectWithData:dbData
                                               options:kNilOptions
                                                 error:nil];
    lessonsList = database[@"lessons"];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.navigationController.toolbarHidden = YES;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return lessonsList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = UITableViewCell.new;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = cell.contentView.backgroundColor =
    [UIColor clearColor];
    
    UIImageView *img = UIImageView.new;
    img.frame = CGRectMake(10, 5, MainScreenWidth-20.0, 50);
    img.image = [UIImage imageNamed:@"GR_main_cell_bg"];
    [cell addSubview:img];
    
    UILabel *titleLbl = [UILabel new];
    titleLbl.text = lessonsList[indexPath.row][@"name"];
    titleLbl.font = [UIFont fontWithName:@"TheSans" size:15.0];
    titleLbl.textColor = [UIColor whiteColor];
    titleLbl.textAlignment = NSTextAlignmentRight;
    CGFloat leftTitle = (indexPath.row < 10)? 100.0 : 115.0;
    titleLbl.frame = CGRectMake(10, 7, MainScreenWidth-(leftTitle+10.0), 50);
    
    UILabel *subTitleLbl = [UILabel new];
    subTitleLbl.text = lessonsList[indexPath.row][@"subtitle"];
    subTitleLbl.font = [UIFont fontWithName:LWAppFontName size:11.0];
    subTitleLbl.textColor = [UIColor colorWithRed:0.086275 green:0.258824 blue:0.388235 alpha:1.0];
    subTitleLbl.text = [subTitleLbl.text stringByAppendingString:@" |"];
    subTitleLbl.textAlignment = NSTextAlignmentLeft;
    CGFloat leftSubTitle = CGRectGetMaxX(titleLbl.frame)+5;
    subTitleLbl.frame = CGRectMake(leftSubTitle, 7, MainScreenWidth-leftSubTitle, 50);
    
    
    [cell addSubview:titleLbl];
    [cell addSubview:subTitleLbl];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LWDetailsCVC *detailsView = LWDetailsCVC.new;
    detailsView.db = database;
    NSArray *pages = database[@"pages"];
    NSInteger pageIndex = -1;
    while(++pageIndex < pages.count)
    {
        for (NSDictionary *section in pages[pageIndex][@"sections"])
        {
            if ([section[@"lesson_index"] integerValue] == indexPath.row)
            {
                detailsView.page = pageIndex;
                [self.navigationController pushViewController:detailsView
                                                     animated:YES];

                return;
            }
        }
    }
}

@end
