//
//  LWDetailsSectionHeader.h
//  golden_rule
//
//  Created by Khaled Qudwa on 8/13/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWDetailsSectionHeader : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *sectionTitle;
@property (weak, nonatomic) IBOutlet UIImageView *upperImg;
@property (weak, nonatomic) IBOutlet UIImageView *lowerImg;

@end
