//
//  LWDetailsCell.h
//  golden_rule
//
//  Created by Khaled Qudwa on 8/13/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWDetailsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellImg;

@end
