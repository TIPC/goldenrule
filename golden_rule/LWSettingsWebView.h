//
//  LWSettingsWebView.h
//  golden_rule
//
//  Created by Khaled Qudwa on 8/10/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LWSettingsWebView : UIViewController
@property NSString *naviTitle;
@property NSString *fileName;
@end
