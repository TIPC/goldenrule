//
//  LWDetailsSectionHeader.m
//  golden_rule
//
//  Created by Khaled Qudwa on 8/13/15.
//  Copyright (c) 2015 TIPC. All rights reserved.
//

#import "LWDetailsSectionHeader.h"
#import "config.h"
@implementation LWDetailsSectionHeader

- (void)awakeFromNib {
    self.lowerImg.transform = CGAffineTransformMakeScale(1, -1);
    self.sectionTitle.textColor = [UIColor whiteColor];
    self.sectionTitle.font = [UIFont fontWithName:LWAppFontName
                                             size:15.0];
    self.sectionTitle.backgroundColor = LWAppMainColor;
    // Initialization code
}

@end
